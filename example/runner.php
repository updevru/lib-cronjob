<?php

require __DIR__ . '/../vendor/autoload.php';

class Test extends \Cronjob\AbstractJob
{
    public function run()
    {
        $this->out('Test OK!');
    }
}

$runner = new \Cronjob\Runner($argv, '', '/tmp/cronjob');
$runner->run();