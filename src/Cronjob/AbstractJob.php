<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 24.02.14
 * Time: 6:41
 */

namespace Cronjob;

abstract class AbstractJob {
    /**
     * @var array
     */
    protected $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getParam($key, $default = null)
    {
        return (isset($this->params[$key])) ? $this->params[$key] : $default;
    }

    abstract public function run();

    protected function out($str)
    {
        print $str . "\n";
    }
} 