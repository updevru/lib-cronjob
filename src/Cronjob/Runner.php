<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 24.02.14
 * Time: 7:04
 */

namespace Cronjob;


use Cronjob\Exception\CronjobException;

class Runner {

    /**
     * @var array
     */
    protected $params;

    /**
     * @var string
     */
    protected $classNamespace;

    protected $pidDirectory;

    protected $lockFileName;

    public function __construct(array $params, $classNamespace, $pidDirectory)
    {
        unset($params[0]);
        foreach($params as $param) {
            $param = trim(trim($param), '-');
            list($key, $value) = explode('=', $param);
            $this->params[$key] = $value;
        }

        $this->classNamespace = $classNamespace;
        $this->pidDirectory = $pidDirectory;

        register_shutdown_function(array($this, 'shutdownHandler'));
    }

    public function run()
    {
        $this->out("Script start ".date('d.m.Y h:i:s'));

        if (!isset($this->params['tool'])) {
            throw new CronjobException("Tool not found");
        }

        $class = $this->classNamespace.$this->params['tool'];
        if(!class_exists($class)) {
            throw new CronjobException("Class '$class' not found");
        }

        try {
            $this->preStartEvent();
        } catch(Exception\GlobalLock $e) {
            $this->out($e->getMessage());
            return;
        }

        /** @var AbstractJob $tool */
        $tool = new $class($this->params);

        try {
            $tool->run();
        } catch (\Exception $e) {
            $this->postStartEvent();
            throw $e;
        }

        $this->postStartEvent();
    }

    protected function preStartEvent()
    {
        $dir = $this->pidDirectory;

        if(!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        $file = "";
        foreach($this->params as $name => $value) {
            $file .= '_'.$name.'_'.str_replace('/', '', $value);
        }
        $this->lockFileName = $dir.'/'.$file.'.cronjob';

        if(is_file($this->lockFileName)) {
            throw new Exception\GlobalLock('Global lock file - ' . $this->lockFileName);
        } else {
            file_put_contents($this->lockFileName, date('d-m-Y H:i:s'));
        }
    }

    public function shutdownHandler()
    {
        $error = error_get_last();
        if (!in_array($error['type'], array(E_ERROR, E_PARSE, E_CORE_ERROR, E_COMPILE_ERROR))) {
            return;
        }
        $this->postStartEvent();
    }

    protected function postStartEvent()
    {
        if(is_file($this->lockFileName)) {
            unlink($this->lockFileName);
        }
        $this->out("Script stop ".date('d.m.Y h:i:s').' Memory: '.round(memory_get_usage()/1024/1024, 2).'MB');
    }

    protected function out($str)
    {
        print $str . "\n";
    }
} 